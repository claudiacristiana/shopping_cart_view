'use strict';

/**
 * @ngdoc overview
 * @name shoppingCartViewApp
 * @description
 * # shoppingCartViewApp
 *
 * Main module of the application.
 */
angular
  .module('shoppingCartViewApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.grid'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/listCart.html',
        controller: 'listCartCtrl',
        controllerAs: 'listcart'
      })
      .when('/add', {
        templateUrl: 'views/addCart.html',
        controller: 'addCartCtrl',
        controllerAs: 'addcart'
      })
      .when('/list', {
        templateUrl: 'views/listProducts.html',
        controller: 'listProductsCtrl',
        controllerAs: 'listProducts'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
