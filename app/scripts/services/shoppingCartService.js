(function () {

  angular.module('shoppingCartViewApp')
    .service('ShoppingCartService', ['$http', ShoppingCartService]);

  function ShoppingCartService($http) {

    var self = this;
    var baseUrl = '';


    self.list = function () {
      return $http({
        method: 'GET',
        url: baseUrl +"getListShoppingCart"
      }).then(function (response) {
        console.log(response.data);
        return response.data;

      });
    };

    self.listProducts = function () {
      return $http({
        method: 'GET',
        url: baseUrl +"getListProducts"
      }).then(function (response) {
        console.log(response.data);
        return response.data;

      });
    };

    self.listEstimationProducts = function () {
      return $http({
        method: 'GET',
        url: baseUrl +"getEstimationProducts"
      }).then(function (response) {
        console.log(response.data);
        return response.data;

      });
    };

    self.add = function (data) {
      return $http({
        method: 'POST',
        url: baseUrl + "addShoppingCart",
        data: data
      }).then(function (response) {
        console.log("ADAUGARE CU SUCCES");
        window.location.href ='/';
        return response.data;
      });
    };
  }
}());
