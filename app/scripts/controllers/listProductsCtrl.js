'use strict';

angular.module('shoppingCartViewApp')
  .controller('listProductsCtrl', ['ShoppingCartService',  '$scope', function (ShoppingCartService, $scope) {
    $scope.gridOptions = {
    };

    $scope.list = function () {
      $scope.gridOptions["columnDefs"] = [
        { name:  'product_name',
          field: 'productName'
        },
        { name:  'auxProductNo',
          field: 'auxProductNo'
        },
        { name:  'totalPrice',
          field: 'total_packet_no'
        },
        { name:  'totalPrice',
          field: 'totalPrice',
          cellFilter: 'number: 2'
        }
        ];

      ShoppingCartService.listEstimationProducts().then(function (response) {
        $scope.gridOptions.data = response.data;
      });
    };
    $scope.list();
  }]);
