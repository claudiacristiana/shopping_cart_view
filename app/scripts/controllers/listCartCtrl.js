'use strict';

angular.module('shoppingCartViewApp')
  .controller('listCartCtrl', ['ShoppingCartService',  '$scope', function (ShoppingCartService, $scope) {
  $scope.gridOptions = {
  };

  $scope.list = function () {
    $scope.gridOptions["columnDefs"] = [
      { name:  'timestamp',
        field: 'timestamp_cart',
        width: 150
      },
      { name:  'product_name',
        field: 'product_name'
      },
      { name:  'total_packet_no',
        field: 'total_packet_no'
      },
      { name:  'total_product_no',
        field: 'total_product_no'
      },
      { name:  'total_price',
        field: 'total_price',
        cellFilter: 'number: 2'
      }];

    ShoppingCartService.list().then(function (response) {
      $scope.gridOptions.data = response.data;
    });
  };
    $scope.list();
}]);
