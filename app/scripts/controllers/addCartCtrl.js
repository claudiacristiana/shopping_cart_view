'use strict';

/**
 * @ngdoc function
 * @name shoppingCartViewApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the shoppingCartViewApp
 */
angular.module('shoppingCartViewApp')
  .controller('addCartCtrl', ['ShoppingCartService',  '$scope', function (ShoppingCartService, $scope) {

    $scope.products =[{product_name : "", id_product : 1}];

    $scope.listProducts = function () {
      ShoppingCartService.listProducts().then(function (response) {
        $scope.products = response.data;
      });
    };
    $scope.listProducts();


    $scope.addCart = function (formIsValid) {
      if (formIsValid){
        $scope.isLoading = true;
      }


      var doForm = function () {
        if (formIsValid) {
          ShoppingCartService.add(
            {
              "auxProductNo": $scope.auxProductNo,
              "auxPacketNo": $scope.auxPacketNo,
              "auxIdProduct": $scope.selectedProduct.id_product
            }
          );
        } else {
          return;
        }
      };


      doForm();


    };

  }]);
